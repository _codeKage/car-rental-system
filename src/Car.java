
public class Car {
	private String brand,model,color;
	private boolean availability;
	private double rentalPricePerDay;
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public boolean isAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	public double getRentalPricePerDay() {
		return rentalPricePerDay;
	}
	public void setRentalPricePerDay(double rentalPricePerDay) {
		this.rentalPricePerDay = rentalPricePerDay;
	}
	public Car(String brand, String model, String color, boolean availability, double rentalPricePerDay) {
		this.brand = brand;
		this.model = model;
		this.color = color;
		this.availability = availability;
		this.rentalPricePerDay = rentalPricePerDay;
	}
	
}
