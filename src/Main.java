import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	private static Scanner sc = new Scanner(System.in);
	private static ArrayList<Car> carList = new ArrayList<Car>();
	private static ArrayList<Customer> customerList = new ArrayList<Customer>();
	private static ArrayList<Rental> rents = new ArrayList<Rental>();
	public static void main(String[] args) {
		ShowWelcomeMessage();
		do {
			int userLevel = LoginUser(); 
			if(userLevel==1) {
				int staffChoice =0;
				do {
					System.out.println("[1]Search Customer [2]Search Vehicle [3]Available Vehicles [4]Release Vehicle [5]Receive Vehicle [6]Logout");
					staffChoice = sc.nextInt();
					switch (staffChoice) {
					case 1:
						SearchCustomer();
						break;
					case 2: 
						SearchVehicle();
						break;
					case 3: 
						ShowAvailableVehicles();
						break;
					case 4:
						ReleaseVehicle();
						break;
					case 5:
						ReceiveVehicle();
						break;
					case 6: 
						System.out.println("Logging out ...");
						break;
					default:
						System.out.println("Invalid Input");
						break;
					}
				}while(staffChoice!=6);
			}else if(userLevel==2) {
				int adminChoice =0;
				do {
				System.out.println("[1]Add Customer [2]Delete Customer [3]Update Customer [4]Add Vehicle [5]Delete Vehicle [6]Update Vehicle [7]Generate Reports [8]Logout");
				adminChoice = sc.nextInt();
				switch(adminChoice) {
					case 1:
						AddCustomer();
						break;
					case 2:
						DeleteCustomer();
						break;
					case 3:
						UpdateCustomer();
						break;
					case 4:
						AddVehicle();
						break;
					case 5:
						DeleteVehicle();
						break;
					case 6:
						UpdateVehicle();
						break;
					case 7:
						GenerateRentalReports();
						break;
					case 8:
						System.out.println("Logging out of the system...");
						break;
					default:
						System.out.println("Invalid Choice");
						break;
					}
				}while(adminChoice!=8);
			}else if(userLevel==0){
				System.out.println("Invalid Username and/or Password");
			}
		}while(1==1);
	}
	private static void ShowWelcomeMessage() {
		System.out.println("******WELCOME TO CAR RENTAL SYSTEM******");
	}
	// User Login
	private static int LoginUser() {
		System.out.print("Username: ");
		String username = sc.next();
		System.out.print("Password: ");
		String password = sc.next();
		if(username.equalsIgnoreCase("staff") && password.equalsIgnoreCase("staff")) {
			//User if a staff
			return 1;
		}
		else if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {
			//User is an admin
			return 2;
		}else {
			//Invalid Username and password
			return 0;
		}
	}
	
	private static void AddCustomer() {
		System.out.print("Enter Customer First Name: ");
		String customerFirstName = sc.next();
		System.out.print("Enter Customer Last Name: ");
		String customerLastName = sc.next();
		System.out.print("Enter Customer Middle Name: ");
		String customerMiddleName = sc.next();
		System.out.print("Enter Customer Address: ");
		String customerAddress = sc.next();
		//Declaration of customer
		Customer customer = new Customer(customerFirstName, customerLastName, customerMiddleName, customerAddress);
		customerList.add(customer);
	}
	private static void AddVehicle() {
		System.out.print("Enter Vechicle Brand: ");
		String brand = sc.next();
		System.out.print("Enter Vehicle Model: ");
		String model = sc.next();
		System.out.print("Enter Vehicle Color: ");
		String color = sc.next();
		System.out.print("Enter The Rental Price for Vehicle: ");
		double rentalPrice = sc.nextDouble();
		Car car = new Car(brand, model, color, true, rentalPrice);
		carList.add(car);
	}
	
	private static void SearchCustomer() {
		System.out.print("Please Enter Customer Last Name: ");
		String customerLastName = sc.next();
		for(int i=0;i<customerList.size();i++) {
			if(customerLastName.equalsIgnoreCase(customerList.get(i).getLastName())) {
				System.out.println((i+1)+". Customer Name: " + customerList.get(i).getFirstName() + " " + customerList.get(i).getMiddleName()+" "+customerList.get(i).getLastName());
			}
		}
		
	}
	private static void ShowAllCustomers() {
		int ctr=1;
		for(Customer customer : customerList) {
			System.out.println( ctr+ " Name: " + customer.getFirstName() + " " + customer.getLastName());
			ctr++;
		}
	}
	private static void SearchVehicle() {
		System.out.print("Please Enter Vehicle Brand or Model: ");
		String vehicleBrand = sc.next();
		for(int i=0;i<customerList.size();i++) {
			if(vehicleBrand.equalsIgnoreCase(carList.get(i).getBrand()) || vehicleBrand.equalsIgnoreCase(carList.get(i).getModel())) {
				System.out.println((i+1) + ". Brand: " + carList.get(i).getBrand()+ "\n Model: " + carList.get(i).getModel()+"\n Color: "+carList.get(i).getColor() + "\n Rental Price per day: " + carList.get(i).getRentalPricePerDay());
			}
		}
	}
	private static void ShowAvailableVehicles() {
		for(int i=0;i<carList.size();i++) {
			Car car = carList.get(i);
			if(car.isAvailability()) {
				System.out.println((i+1) +". Brand :" + car.getBrand()+"\n Model: " + car.getModel() + "\n Color: " + car.getColor());
			}					
		}
	}
	private static void ReceiveVehicle() {
		try {
		System.out.println("Which Vechicle to Receive?");
		for(int x=0;x<rents.size();x++) {
			System.out.println((x+1)+" Car: " + carList.get(rents.get(x).getCarId()).getBrand() +" " + carList.get(rents.get(x).getCarId()).getModel() );
			System.out.println("Customer: "+ customerList.get(rents.get(x).getCustomerId()).getFirstName() + " " + customerList.get(rents.get(x).getCustomerId()).getLastName());
		}
		System.out.println("Enter the Transaction number: ");
		int transNumber = sc.nextInt()-1;
		rents.get(transNumber).setReturned(true);
		carList.get(rents.get(transNumber).getCarId()).setAvailability(true);
		System.out.println("Vehicle Received Successfully");
		}catch (Exception e) {
			System.out.println("Invalid Operation");
		}
	}
	
	private static void ReleaseVehicle() {
		try {
		System.out.println("Select Vehicle to Release: ");
		ShowAvailableVehicles();
		System.out.print("Vehicle ID: ");
		int carId = sc.nextInt()-1;
		if(carList.get(carId).isAvailability()) {
		System.out.println("Select A Customer: ");
		ShowAllCustomers();
		System.out.print("Customer ID: ");
		int customerId=sc.nextInt()-1;	
		System.out.print("Number of days: ");
		int numOfDays = sc.nextInt();
		Rental rental = new Rental(carId, customerId, numOfDays, false);
		GenerateReleaseSlip(rental);
		GenerateSalesInvoice(rental);
		rents.add(rental);
		System.out.println("Car Rented!");
		carList.get(carId).setAvailability(false);

		}else {
			System.out.println("Car not Available");
		}
		}catch (Exception e) {
			System.out.println("Invalid Vehicle or Customer");
		}
	}
	
	private static void GenerateReleaseSlip(Rental rental) {
		Car car = carList.get(rental.getCarId());
		System.out.println(car.getBrand()+" "+car.getModel() +" was released to: " + customerList.get(rental.getCustomerId()).getFirstName() + " " + customerList.get(rental.getCustomerId()).getLastName());
	}
	
	private static void GenerateSalesInvoice(Rental rental) {
		Car car = carList.get(rental.getCarId());
		Customer customer = customerList.get(rental.getCustomerId());
		System.out.println("-------Sales Invoice-------");
		System.out.println(customer.getFirstName() +" "+customer.getLastName());
		System.out.println(car.getBrand()+" "+ car.getColor() +" : " +car.getRentalPricePerDay());
		System.out.println("Number of days: " + rental.getNumOfDays());
		System.out.println("Total Price: " + (car.getRentalPricePerDay()*rental.getNumOfDays()));
	}
	
	private static void UpdateVehicle() {
		try {
			ShowAvailableVehicles();
		System.out.println("Enter the Vehicle Number to Update: ");
		int carId = sc.nextInt()-1;
		System.out.print("Enter Car Brand: ");
		carList.get(carId).setBrand(sc.next());
		System.out.print("Enter Car Model: ");
		carList.get(carId).setBrand(sc.next());
		System.out.print("Enter Car Color: ");
		carList.get(carId).setColor(sc.next());
		System.out.print("Rent Price per Day: ");
		carList.get(carId).setRentalPricePerDay(sc.nextDouble());
		System.out.println("\n Car updated successfully!");
		}catch (Exception e) {
			System.out.println("Invalid operation");
		}
	}
	private static void DeleteVehicle() {
		try {
		System.out.println("Select a Vehicle to Delete");
		ShowAvailableVehicles();
		System.out.print("Vehicle Number: ");
		int vehichleNumber = sc.nextInt()-1;
		if(CheckDeletableVehicle(vehichleNumber)) {
			carList.remove(vehichleNumber);
			System.out.println("Vehicle Removed");
		}else {
			System.out.println("Cannot Remove Vehicle,already in a transaction");
		}
		}catch (Exception e) {
			System.out.println("Invalid Operation");
		}
	}
	private static void UpdateCustomer() {
		try {
		ShowAllCustomers();
		System.out.print("Enter the Customer Number to Update: ");
		int customerNumber = sc.nextInt()-1;
		System.out.print("Enter Customer First Name: ");
		customerList.get(customerNumber).setFirstName(sc.next());
		System.out.print("Enter Customer Last Name: ");
		customerList.get(customerNumber).setLastName(sc.next());
		System.out.print("Enter Customer Middle Name: ");
		customerList.get(customerNumber).setAddress(sc.next());
		System.out.print("Customer Updated");
		}catch (Exception e) {
			System.out.println("Invalid Operation");
		}
	}
	private static boolean CheckDeletableVehicle(int carId) {
		boolean x = true;
		for(Rental rent: rents) {
			if(rent.getCarId()==carId) {
				x = false;
			}
		}
		return x;
	}
	private static boolean CheckDeletableCustomer(int customerNumber) {
		boolean x=true;
		for(Rental rent : rents) {
			if(rent.getCustomerId()==customerNumber) {
				x=false;
			}
		}
		return x;
	}
	private static void DeleteCustomer() {
		try {
		ShowAllCustomers();
		System.out.print("Customer Number: ");
		int customerNumber = sc.nextInt()-1;
		if(CheckDeletableCustomer(customerNumber)) {
			customerList.remove(customerNumber);
			System.out.println("Successfully Deleted Customer");
		}else {
			System.out.println("Cannot Delete customer, already in a transaction");
		}
		}catch (Exception e) {
			System.out.println("Invalid Operation");
		}
	}
	private static void GenerateRentalReports() {
		System.out.println("Number of Transactions: " + rents.size());
		for(int x=0;x<rents.size();x++) {
			Rental rent = rents.get(x);
			Car car = carList.get(rent.getCarId());
			Customer customer = customerList.get(rent.getCustomerId());
			System.out.println("Borrower: " + customer.getFirstName()+" " + customer.getLastName());
			System.out.println("Car: " + car.getBrand()+" "+car.getModel()+" "+car.getColor());
			System.out.println("Number of days: " + rent.getNumOfDays());
			System.out.println("Total Amount: " + (rent.getNumOfDays()*car.getRentalPricePerDay()));
		}
		
	}
	
}
