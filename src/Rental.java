
public class Rental {
	private int carId,customerId,numOfDays;
	private boolean isReturned;

	public Rental(int carId, int customerId, int numOfDays,boolean isReturned) {
		this.carId = carId;
		this.customerId = customerId;
		this.numOfDays = numOfDays;
		this.isReturned = isReturned;
	}

	public boolean isReturned() {
		return isReturned;
	}

	public void setReturned(boolean isReturned) {
		this.isReturned = isReturned;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getNumOfDays() {
		return numOfDays;
	}

	public void setNumOfDays(int numOfDays) {
		this.numOfDays = numOfDays;
	}
}
